# Build strings in Golang binary
#@author Vincent Brillault, Nikolaos Filippakis
#@category CERN.Golang
#@keybinding
#@menupath
#@toolbar

from ghidra.program.model.symbol.SourceType import *
import sys
import string

memory = currentProgram.getMemory()
referenceManager = currentProgram.getReferenceManager()
dataTypeManager = currentProgram.getDataTypeManager()
listing = currentProgram.getListing()

STRING = dataTypeManager.getDataType("/string")


def get_segment(name):
    segments = memory.getBlocks()
    for seg in segments:
        if seg.getName() == name:
            return seg
    print "Segment %s not found" % name
    sys.exit(-1)


def get_reference_ranges(segment):
    start = segment.getStart()
    end = segment.getEnd()
    pos = start.next()
    while pos < end:
        if referenceManager.getReferenceCountTo(pos) > 0:
            yield (start, pos.previous())
            start = pos
        pos = pos.next()
    yield (start, end)


def range_printable(start, end):
    while start <= end:
        b = memory.getByte(start)
        if not (b in range(256) and chr(b) in string.printable):
            return False
        start = start.next()
    return True


def makeString(addr, addr_to):
    listing.clearCodeUnits(addr, addr_to, False)
    listing.createData(addr, STRING, addr_to.subtract(addr)+1)
    return listing.getDataAt(addr).value


rodata = get_segment('.rodata')
for (start, end) in get_reference_ranges(rodata):
    if range_printable(start, end):
        value = makeString(start, end)
        print('%s-%s: %s' % (start, end, value))
