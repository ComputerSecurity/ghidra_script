# Build strings in Golang binary
#@author Vincent Brillault
#@category CERN.Golang
#@keybinding
#@menupath
#@toolbar

from ghidra.program.model.symbol.SourceType import *
from ghidra.program.model.scalar import Scalar
import sys
import string

addressFactory = currentProgram.getAddressFactory()
memory = currentProgram.getMemory()
referenceManager = currentProgram.getReferenceManager()
dataTypeManager = currentProgram.getDataTypeManager()
listing = currentProgram.getListing()

STRING = dataTypeManager.getDataType("/string")


def get_segment(name):
    segments = memory.getBlocks()
    for seg in segments:
        if seg.getName() == name:
            return seg
    print "Segment %s not found" % name
    sys.exit(-1)


def range_printable(start, end):
    while start <= end:
        b = memory.getByte(start)
        if not (b in range(256) and chr(b) in string.printable):
            return False
        start = start.next()
    return True


def makeString(start, length, clean_end):
    listing.clearCodeUnits(start, clean_end, False)
    listing.createData(start, STRING, length)
    return listing.getDataAt(start).value

rodata = get_segment('.rodata')

def check_string_load(lea, mov_str, mov_size):
    if lea.getMnemonicString() != 'LEA':
        return (None, None)
    if mov_str.getMnemonicString() != 'MOV':
        return (None, None)
    if mov_size.getMnemonicString() != 'MOV':
        return (None, None)
    if lea.getOpObjects(0) != mov_str.getOpObjects(1):
        return (None, None)
    lea_src = lea.getOpObjects(1)
    if not isinstance(lea_src[0], Scalar):
        return (None, None)
    lea_addr = addressFactory.getAddress(lea_src[0].toString())
    if lea_addr is None or not rodata.contains(lea_addr):
        return (None, None)
    mov_str_dst = mov_str.getOpObjects(0)
    mov_size_dst = mov_size.getOpObjects(0)
    if mov_str_dst[0] != mov_size_dst[0]:
        return (None, None)
    if len(mov_size_dst) < 2:
        return (None, None)
    mov_size_dst_offset = mov_size_dst[1].getSignedValue()
    mov_size_src = mov_size.getOpObjects(1)
    if len(mov_str_dst) < 2:
        if mov_size_dst_offset != 8:
            return (None, None)
    else:
        mov_str_dst_offset = mov_str_dst[1].getSignedValue()
        if mov_size_dst_offset - mov_str_dst_offset != 8:
            return (None, None)
    size = mov_size.getOpObjects(1)
    if not isinstance(size[0], Scalar):
        return (None, None)
    return (lea_addr, size[0].getUnsignedValue())

def check_string(start, length):
    end = start.add(length-1)
    if not range_printable(start, end):
        return False
    datatype_name = 'None'
    data = listing.getDataAt(start)
    if data is not None:
        datatype = data.getDataType()
        if datatype is not None:
            datatype_name = datatype.getName()
    if datatype_name != 'string':
        newStr = makeString(start, length, end)
        print("Found string not seen before (was %s) at %s of len %s: %s" % (datatype_name, start, length, newStr))
        return True
    data_len = data.getLength()
    if data_len > length:
        newStr = makeString(start, length, start.add(data_len-1))
        print("Founds string of wrong length at %s: exisiting %d, real %d: %s" % (start, data_len, length, newStr))
    elif data_len < length:
        newStr = makeString(start, length, end)
        print("Founds string of wrong length at %s: exisiting %d, real %d: %s" % (start, data_len, length, newStr))
    return True

string_seen = []
codeIterator = listing.getCodeUnits(True)
opsa = None
opsb = codeIterator.next()
opsc = codeIterator.next()
while codeIterator.hasNext():
    opsa, opsb, opsc = opsb, opsc, codeIterator.next()
    (start, length) = check_string_load(opsa, opsb, opsc)
    if start != None and length > 0:
        if check_string(start, length):
            string_seen.append((start, length))

string_seen = sorted(set(string_seen))
(prev_start, prev_length) = string_seen[0]
for instance in string_seen[1:]:
    (start, length) = instance
    if start >= prev_start and start < prev_start.add(prev_length):
        print("Found colliding strings in %s (%d) and %s (%d)" % (prev_start, prev_length, start, length))
    prev_start, prev_length = start, length
